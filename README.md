# Flask_Gcloud_RealPython

Flask_Gcloud est un project minimaliste, tiré de ce [tutoriel](https://realpython.com/python-web-applications/) de RealPython.
Il vise à créer et déployer une app Flask avec le service GoogleCloud.

## Test Local

```powershell
python -m pip install -r requirements.txt
python main.py
```

## Test Deploiement sur Gcloud

Après avoir correctement configuré gcloud CLI, vous être authentifié localement (-gcloud auth login), et avoir initialisé un projet sur Google Clood Plateform : 

```powershell
gcloud config set project Nom_Du_Projet
gcloud app deploy
```
